$(document).ready(function() {
    var numberOne = '0';
    var numberTwo = '0';
    var operator = "";
    var resultat = 0;
    var memory = 0;

    displayUpdate(resultat);

    //Память
    $(".btn-memory").on('click', function() {
        if ($(this).attr("value") === "MC") {
            numberOne = '0';
            memory = 0;
        } else if ($(this).attr("value") === "MR") {
            numberOne = memory;
        } else if ($(this).attr("value") === "M+") {
            memory += parseFloat(numberOne);
            numberOne = '0';
        } else if ($(this).attr("value") === "M-") {
            memory -= parseFloat(numberOne);
            numberOne = '0';
        } else if ($(this).attr("value") === "MS") {
            memory = parseFloat(numberOne);
            numberOne = '0';
            //console.log("memory save");
        }
        displayUpdate(numberOne);

    });

    //Получить функцию
    $(".function").on('click', function() {
        if ($(this).attr("value") === "%") {
            numberOne = numberOne / 100;
        } else if ($(this).attr("value") === "√") {
            numberOne = Math.sqrt(numberOne);
        } else if ($(this).attr("value") === "x^2") {
            numberOne *= numberOne;
        } else if ($(this).attr("value") === "1/x") {
            numberOne = 1 / numberOne;
        } else if ($(this).attr("value") === "CE") {
            numberOne = '0';
        } else if ($(this).attr("value") === "C") {
            resultat = 0;
            numberOne = '0';
        } else if (checkNumber($(this).attr("value"))) {
            if (numberOne === '0') numberOne = $(this).attr("value");
            else numberOne = numberOne + $(this).attr("value");
        } else if (checkOperator($(this).attr("value"))) {
            operator = $(this).attr("value");
            numberTwo = parseFloat(numberOne) + operator;
            $("#input").html(numberTwo);
            //operator = $(this).attr("value");
            numberOne = '';
        } else if ($(this).attr("value") === "+/-") {
            numberOne *= -1;
        } else if ($(this).attr("value") === ".") {
            numberOne += '.';
        } else if ($(this).attr("value") === "=") {
            numberOne = equal(numberTwo, numberOne, operator);
            var sbros = '';
            $("#input").html(sbros);
            operator = "";
        }

        displayUpdate(numberOne);
    });
});

//Обновление дисплея
displayUpdate = function(value) {
    value = value.toString();
    $('#display').html(value.substring(0, 15));
};

//Проверка если число
checkNumber = function(value) {
    return !isNaN(value);
};

//Проверка если оператор
checkOperator = function(value) {
    return value === '/' || value === '*' || value === '+' || value === '-';
};

// Функция равно
equal = function(m, n, operator) {
    m = parseFloat(m);
    n = parseFloat(n);
    //console.log(m,operator,n);
    if (operator === '+') resultat = m + n;
    if (operator === '-') resultat = m - n;
    if (operator === '*') resultat = m * n;
    if (operator === '/') resultat = m / n;

    return resultat;
};